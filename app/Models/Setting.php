<?php declare (strict_types = 1);

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    public $timestamps = false;

    protected $filterKey = 'name';

    protected $fillable = [
        'name', 'slug', 'description', 'type', 'value', 'user_id',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo|User
     */
    public function owner()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
