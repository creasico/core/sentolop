<?php declare (strict_types = 1);

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    use ThrottlesLogins;

    public function register(Request $request)
    {
        return [];
    }

    public function login(Request $request)
    {
        return [];
    }

    public function logout(Request $request)
    {
        return [];
    }

    public function reset(Request $request)
    {
        return [];
    }
}
