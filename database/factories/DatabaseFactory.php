<?php

use App\Models;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Models\User::class, function (Faker $fake) {
    static $active_at;

    return [
        'display' => $fake->userName,
        'username' => $fake->userName,
        'email' => $fake->unique()->safeEmail,
        'password' => 'secret',
        'remember_token' => Str::random(10),
        'active_at' => $active_at ?: $active_at = $fake->dateTime,
    ];
});

$factory->define(Models\Role::class, function (Faker $fake) {
    return [
        'slug' => Str::slug($fake->sentence(1), '_'),
        'scopes' => [],
    ];
});

$factory->define(Models\Setting::class, function (Faker $fake) {
    return [
        'slug' => Str::slug($fake->sentence(1), '_'),
        'type' => $fake->sentence(3),
        'value' => null,
    ];
});
