<?php

namespace Tests\Unit\Models;

use App\Models\Role;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class RoleTest extends TestCase
{
    use DatabaseMigrations;

    public function testTranslatableNameAndDescription()
    {
        $role = factory(Role::class)->create([
            'slug' => 'editor'
        ]);

        $this->assertEquals($role->name, trans('roles.editor.name'));
        $this->assertEquals($role->description, trans('roles.editor.description'));
    }

    public function testMakeAsDefault()
    {
        Role::firstOrCreate(['slug' => 'editor'], ['is_default' => true]);
        $contributor = Role::firstOrCreate(['slug' => 'contributor']);

        $this->assertTrue($contributor->makeDefault());

        $this->assertFalse(Role::where('slug', 'editor')->first()->is_default);
        $this->assertTrue($contributor->is_default);
    }
}
