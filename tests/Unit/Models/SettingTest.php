<?php

namespace Tests\Unit\Models;

use App\Models\Setting;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class SettingTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * @var Setting|Setting[]|Collection
     */
    protected $settings;

    protected function setUp(): void
    {
        parent::setUp();

        $this->settings = Setting::count() == 0 ? factory(Setting::class, 2)->create() : Setting::all();
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->assertTrue(true);
    }
}
