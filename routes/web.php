<?php

Route::get('/{view}', function () {
    return view('main');
})->where('view', '.*')->name('web.view');
