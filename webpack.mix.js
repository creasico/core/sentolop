/* global Mix */

const { resolve } = require('path')
const mix = require('laravel-mix')
const { spawn } = require('child_process')

// Start Artisan Serve

let serve

Mix.listen('init', () => {
  // Only start the server if on HMR
  if (!mix.config.hmr) return

  serve = spawn('php', ['artisan', 'serve'])

  function logOutput (data) {
    console.info(data.toString().trim())
  }

  serve.stdout.on('data', logOutput)
  serve.stderr.on('data', logOutput)

  serve.on('close', (code) => {
    console.info('Server closed with status:', code)
  })
})

process.on('exit', () => {
  if (serve) serve.kill('SIGKILL')
})

// Configure BrowserSync

mix.browserSync({
  open: false,
  notify: false,
  proxy: 'localhost:8000',
  // see https://www.browsersync.io/docs/options#option-serveStatic
  serveStatic: [
    {
      route: '/storage',
      dir: 'storage/app/public'
    }
  ]
})

mix.webpackConfig({
  node: {
    fs: 'empty'
  },
  module: {
    rules: [
      {
        test: /\.(vue|js)$/,
        loader: 'eslint-loader',
        enforce: 'pre',
        exclude: /node_modules/
      },
      {
        test: /\.less$/,
        loader: 'less-loader',
        options: {
          javascriptEnabled: true
        }
      }
    ]
  },
  resolve: {
    alias: {
      '@': resolve(__dirname, 'resources/client'),
      '@assets': resolve(__dirname, 'resources/assets')
    }
  }
})

mix.options({
  postCss: [
    require('autoprefixer')(),
    require('cssnano')({
      preset: ['default']
    })
  ],
  processCssUrl: true,
  extractVueStyles: 'css/main.css',
  // globalVueStyles: 'resources/assets/styles/global.less',
  vue: {
    /**
     * @link https://bootstrap-vue.js.org/docs/reference/images/
     */
    transformAssetUrls: {
      video: ['src', 'poster'],
      source: 'src',
      img: 'src',
      image: 'xlink:href',
      'b-img': 'src',
      'b-img-lazy': ['src', 'blank-src'],
      'b-card': 'img-src',
      'b-card-img': 'img-src',
      'b-carousel-slide': 'img-src',
      'b-embed': 'src'
    }
  }
})

mix
  .copy('resources/assets/favicons', 'public/favicons')
  .js('resources/client/main.js', 'public/js')
  .disableNotifications()
  .extract()

if (mix.inProduction()) {
  mix.version()
} else {
  mix.sourceMaps()
}
