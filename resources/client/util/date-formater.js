import fnTimesAgo from 'date-fns/distance_in_words_to_now'
import fnFormat from 'date-fns/format'
import locale from 'date-fns/locale/en'

export function format (date, format, options = {}) {
  return fnFormat(date, format, { locale, ...options })
}

export function timesAgo (date, options = {}) {
  return fnTimesAgo(date, { locale, ...options })
}
