
/**
 * Get site meta.
 *
 * @param  {String}  name  Site meta name.
 * @param  {any}  defaults  Site meta name.
 * @return {string|undefined}
 */
export const getMeta = (name, defaults = null) => {
  const meta = document.head.querySelector(`meta[name="${name}"]`)

  return meta ? meta.content : defaults
}
