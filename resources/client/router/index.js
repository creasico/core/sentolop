import Qs from 'qs'
import Vue from 'vue'
import VueRouter from 'vue-router'

import { getMeta } from '@/util'
import routes from './routes'

Vue.use(VueRouter)

const queryOptions = {
  indices: true,
  allowDots: true
}

export default new VueRouter({
  base: getMeta('base-url', '/'),
  mode: 'history',
  linkActiveClass: 'active',
  linkExactActiveClass: 'active',
  routes,
  scrollBehavior (to, from, savedPosition) {
    if (to.hash) {
      return {
        selector: to.hash
      }
    }

    if (savedPosition) {
      return savedPosition
    }

    return { x: 0, y: 0 }
  },
  parseQuery (query) {
    return Qs.parse(query, queryOptions)
  },
  stringifyQuery (query) {
    const result = Qs.stringify(query, queryOptions)

    return result ? `?${result}` : ''
  }
})
